#include <stdio.h>
#include <string.h>

int main(int argc, char** argv) {
	if(argc==2 && !(strcmp("-h", argv[1]) && strcmp("--help", argv[1]))) {
		printf("%s \e[32m%c\e[0m %s\n", "Press", 'f', "to pay respects.");
	} else {
		printf("\e[32m%s\e[0m\n", "Respects paid.");
	}
	return 0;
}

