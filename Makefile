.PHONY: build install homeinstall help all clean mrproper

CC = gcc
RM = rm -f
INSTALL = install
INSTALLDOC = install -m 400
STRIP = strip
MKDIR = mkdir -p

all: build

%.o: %.c
	$(CC) $^ -o $@ -c

f: respects.o
	$(CC) $^ -o $@
	$(STRIP) f

build: f

clean:
	$(RM) *.o

mrproper: clean
	$(RM) f

install: f
	$(INSTALL) f /usr/local/bin
	$(INSTALLDOC) doc/f.1 /usr/local/share/man/man1
	makewhatis /usr/local/share/man

homeinstall: $(HOME)/bin $(HOME)/share/man/man1 f
	$(INSTALL) f $(HOME)/bin
	$(INSTALLDOC) doc/f.1 $(HOME)/share/man/man1
	makewhatis $(HOME)/share/man

help:
	@echo To build, use make build
	@echo To install for all users, use make install
	@echo To install for you, use make homeinstall

$(HOME)/bin:
	$(MKDIR) $@

$(HOME)/share/man/man1:
	$(MKDIR) $@
